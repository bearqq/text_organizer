#coding: utf-8
#python2
import sys,re
from appJar import gui


app=gui()
app.winIcon = None #fix ico missing

if sys.getdefaultencoding() != 'utf8':
    reload(sys)
    sys.setdefaultencoding( "utf-8" )

class intgrow(object):
    def __init__(self, *args):
        self.i=0
        self.m=0
        
    
    @property
    def A(self):
        self.i+=1
        return self.i-1
    
    @property
    def D(self):
        self.i-=1
        return self.i+1

    @property
    def S(self):
        return self.i
    
    def P(self,i):
        self.i+=i
    
    @property
    def R(self):
        if self.m<self.i:
            self.m=self.i
        self.i=0
    
    @property
    def M(self):
        if self.m<self.i:
            self.m=self.i
        else:
            self.i=self.m

            
def B_space(btm):
    s=app.getTextArea("textmain").replace(' ','')
    app.clearTextArea("textmain")
    app.setTextArea("textmain", s)

def B_enter(btm):
    #s=app.getTextArea("textmain").replace('\r','').replace('\n','')
    def smart_enter(matchobj):
        smach=matchobj.group(0)
        if len(smach)>1 and (smach[-2] in ['.',u'。',u'．']):
            return smach
        else:
            return smach[:-1]
    s=re.sub(u'.{1}\r',smart_enter,app.getTextArea("textmain"))
    s=re.sub(u'.{1}\n',smart_enter,s)
    app.clearTextArea("textmain")
    app.setTextArea("textmain", s)

def B_ref(btm):
    """
    def smart_ref(matchobj):
        smach=matchobj.group(0)
        print smach
        return ''
        if len(smach)>1 and (smach[0] in ['［','[']):
            return ''
        else:
            return smach
    """

    s=re.sub(u'［\d*-*~*\d*］','',app.getTextArea("textmain"))
    s=re.sub('\[\d*-*~*\d*\]','',s)
    s=re.sub(u'【\d*-*~*\d*】','',s)
    app.clearTextArea("textmain")
    app.setTextArea("textmain", s)

def B_comm(btm):
    def smart_comm(matchobj):
        smach=matchobj.group(0)
        if re.search('\d\.\d',smach.replace(u'．','.')):
            return smach[0]+'.'+smach[-1]
        else:
            return smach[0]+u'。'+smach[-1]
    
    def smart_comm2(matchobj):
        smach=matchobj.group(0)
        return u'。'+smach[1:]

    s=re.sub(u'.{1}[\.．].{1}',smart_comm,app.getTextArea("textmain"))
    #s=re.sub(u'.{1}．.{1}',smart_comm,app.getTextArea("textmain"))
    s=re.sub(u'[\.．]\r{0,1}\n',smart_comm2,s)
    s=s.replace(',',u'，').replace(';',u'；').replace(';',u'；').replace(':',u'：')
    if s[-1] in ['.',u'．']:
        s=s[0:-1]+u'。'
    #s=app.getTextArea("textmain").replace(u'．',u'。')
    app.clearTextArea("textmain")
    app.setTextArea("textmain", s)

def B_dash(btm):
    def smart_dash(matchobj):
        smach=matchobj.group(0)
        if len(smach)>1:
            return smach.replace(u'—','-').replace(u'一','-')
        else:
            return ''
    s=re.sub(u'\w{1}—|一\w{1}',smart_dash,app.getTextArea("textmain"))
    app.clearTextArea("textmain")
    app.setTextArea("textmain", s)







def B_Chinese(btm):
    B_space(btm)
    B_enter(btm)
    B_ref(btm)
    B_comm(btm)
    B_dash(btm)


def B_English(btm):
    #B_space(btm)
    B_enter(btm)
    B_ref(btm)
    #B_comm(btm)


def main():

    app.setGeometry(800, 600)
    app.setFont(16,font = ("SimHei"))
    
    app.addScrolledTextArea("textmain")
    #.setTextArea(title, text, end=True, callFunction=True)
    #.getTextArea(title)
    
    app.startSubWindow("control", modal=False)
    row=intgrow()
    col=intgrow()
    app.addButton(u"去空格",  B_space, row.A, col.S)
    app.addButton(u"智能换行",  B_enter, row.A, col.S)
    app.addButton(u"半角全角",  B_comm, row.A, col.S)
    app.addButton(u"去引用[1]",  B_ref, row.A, col.S)
    app.addButton(u"替换-",  B_dash, row.A, col.S)
    
    row.R
    col.A
    app.addButton(u"中文自动",  B_Chinese, row.A, col.S)
    app.addButton(u"英文自动",  B_English, row.A, col.S)
    app.stopSubWindow()
    
    
    app.showSubWindow("control")
    app.go()
    
    



if __name__ == '__main__':
    main()